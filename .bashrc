#!/usr/bin/bash

alias e=$EDITOR
alias ls='ls --color -F'
alias la='ls -A'
alias grep='grep --color'
alias xclip='xclip -sel c'
alias xsel='xsel -b'
alias m='man'

alias gs='git stat'
alias ga='git add'
alias gc='git checkout'
alias gr='git reset'
alias grm='git rm'

compost() {
    mkdir -p "$HOME/.compost"
    for f in "$@"; do
        mv -f $f "$HOME/.compost"
    done
}
alias rm=compost
alias rmm='command rm'

WORDCHARS="${WORDCHARS/\//}" # / is not a wordchar

[ -f ~/.prompt ] && source ~/.prompt
