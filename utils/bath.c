/* The Better pATH shortener */

#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

const char sep = '/';
const char *elide = "...";
const int lenelide = 3;

/* TODO figure out how to handle escaped slashes */

void parseargs(int argc, char **argv, char **path, int *goal) {
	if (argc != 3) {
		fprintf(stderr, "Usage: %s <path> <maximum len>\n", argv[0]);
		exit(1);
	}

	*path = argv[1];
	sscanf(argv[2], "%i", goal);
}

/** Remove the head from a path. Allocates `head`. If `whole` contains only one
 *  segment, it will be moved entirely to `head`.
 *      char *out, *head, *whole = "lorem/ipsum/dolor/sit";
 *      guillotine(&head, whole) == 3;
 *      head == "sit";
 *      whole == "lorem/ipsum/dolor";
 *
 *  @returns The length of `head`
 *  @param head The destination where the last segment of `whole` will be written
 *  @param whole A path, which will have the last segment removed
 * */
int guillotine(char **head, char *whole) {
	char *itr;
	int lenhead;

	for (lenhead = 0, itr = strchr(whole, '\0');
	     *itr != sep && itr > whole;
	     --itr, ++lenhead);
	*head = malloc(sizeof(char) * lenhead);
	strcpy(*head, &itr[1]);
	*itr = '\0';

	return lenhead;
}

/** Reduce parent directories to a single character, until the resulting
 *  abbreviated path is at most `goal` characters long, storing the result in
 *  `abbrev`. The last segment of the path will never be abbreviated, and
 *  therefore it can only be guaranteed that `abbrev` will be at most
 *  `max(goal, strlen(head))` characters long, where `head` is the last segment
 *  of the path.
 *
 *  @returns The length of the abbreviated path
 *  @param abbrev The destination where the abbreviated path will be written
 *  @param path the path to be abbreviated
 *  @param goal The maximum length that `path` will be abbreviated to, if possible.
 */
int abbreviatebydirs(char *abbrev, const char *path, int goal) {
	int rem;
	char *itr;

	if (!*path) {
		*abbrev = '\0';
		return -goal;
	}

	/* Abbreviate /path/to/file/ as /p/to/file as much as necessary and
	 * possible (conditions in that order) */
	for (rem = strlen(path), itr = abbrev;
	     itr - abbrev + rem > goal && *path;
	     *(itr++) = sep) {
		for (*(itr++) = *(path++); *(path++) != sep && *path; --rem);
	}

	/* Assuming there are unabbreviated leftovers, copy those to abbrev */
	if (*path == sep) ++path;
	while (( *(itr++) = *(path++) ));
	if (itr[-2] == sep) {
		itr[-2] = '\0';
	}

	/* Positive retval    =:= how much  longer the abbreviation is than is acceptable
	 * Nonpositive retval =:= how much shorter the abbreviation is than the cap */
	return itr - abbrev - goal;
}

void abbreviate(char *path, int goal) {
	char *abbrev, *head;
	int lenhead, overflow;
	bool abspath = false;

	if (strlen(path) <= goal)
		return;

	abbrev = malloc(sizeof(char) * (strlen(path) + 1)); /* +1 is for null char */
	/* Never abbreviate the final part of the path */
	lenhead = guillotine(&head, path);

	if (path[0] == sep) {
		++path;
		--goal;
		abspath = true;
	}

	/* If abbreviating /path/to/file -> /p/t/file isn't enough, grab the
	 * end of path and mark the elision. However, never abbreviate head. */
	if ((overflow = abbreviatebydirs(abbrev, path, goal - lenhead)) <= 1) 
		sprintf(path, "%s/%s\n", abbrev, head);
	else if (lenelide < goal + overflow - lenhead)
		sprintf(path, "%s%s/%s\n", elide, &abbrev[overflow + lenelide], head);
	else
		sprintf(path, "%s\n", head);

	if (abspath) --path;

	free(abbrev);
	free(head);
}

int main(int argc, char **argv) {
	char *path;
	int goal;

	parseargs(argc, argv, &path, &goal);

	abbreviate(path, goal);
	printf("%s", path);

	return 0;
}
