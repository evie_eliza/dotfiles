try-source() {
    [[ -f "$1" ]] && source "$1"
}

try-source "$HOME/.bashrc"
try-source "$HOME/.prompt"
try-source "/usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
try-source "/usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh"

setopt AUTO_PARAM_SLASH
setopt CDABLE_VARS
setopt COMPLETE_ALIASES
setopt CORRECT
setopt HISTIGNOREDUPS
setopt LIST_ROWS_FIRST
setopt MARKDIRS
setopt PROMPT_SUBST

zstyle ':completion:*' menu select
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion::complete:cd::' local-files
autoload -Uz compinit
compinit
compdef rmm=rm

SPROMPT="Correct spelling: %B%F{red}%R%f -> %F{blue}%r%f%b? [nyae]"

HISTFILE=~/.histfile
HISTSIZE=1024
SAVEHIST=1024

bindkey -e
bindkey -s '^[;' '~/'

try-source "$HOME/.fzf.zsh"
