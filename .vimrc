" Import plugins {{{
let g:plug_dir = expand('$HOME/.vim/plugged')
if isdirectory(g:plug_dir)
    call plug#begin(g:plug_dir)

    " Plug 'EESchneider/vim-rebase-mode'
    Plug 'EESchneider/xueer'
    Plug 'ap/vim-buftabline'
    Plug 'eraserhd/parinfer-rust', {'do': 'cargo build --release'}
    Plug 'godlygeek/tabular'
    Plug 'idris-hackers/idris-vim'
    Plug 'junegunn/fzf', {'do': {-> fzf#install()}}
    Plug 'junegunn/fzf.vim'
    Plug 'kana/vim-arpeggio'
    Plug 'kana/vim-textobj-user'
    Plug 'lervag/vimtex'
    Plug 'machakann/vim-sandwich'
    Plug 'majutsushi/tagbar'
    Plug 'neovimhaskell/haskell-vim', { 'for': 'haskell' }
    Plug 'preservim/vim-textobj-quote'
    Plug 'romainl/vim-qf'
    Plug 'rrethy/vim-hexokinase', { 'do': 'make hexokinase'}
    Plug 'tmsvg/pear-tree'
    Plug 'tpope/vim-commentary'
    Plug 'tpope/vim-dispatch'
    Plug 'tpope/vim-fugitive'
    Plug 'tpope/vim-repeat'
    Plug 'tpope/vim-sleuth'
    Plug 'wellle/targets.vim'

    if has('nvim')
        Plug 'lewis6991/gitsigns.nvim'
        " vim-lsp-settings significantly slows startup time, and I don't use LSP very much.
        " If the performance issue can be fixed it would be nice to have.
        " Plug 'prabirshrestha/vim-lsp'
        " Plug 'mattn/vim-lsp-settings'
        Plug 'olical/conjure'
        Plug 'olical/nfnl'
        Plug 'olical/aniseed'
        Plug 'phaazon/hop.nvim'
        Plug 'radenling/vim-dispatch-neovim'
        Plug 'karb94/neoscroll.nvim'
    else
        Plug 'airblade/vim-gitgutter'
        Plug 'easymotion/vim-easymotion'
        Plug 'tpope/vim-fireplace'
        Plug 'tpope/vim-salve'
    endif

    " pretty colors
    Plug 'haishanh/night-owl.vim'
    Plug 'ayu-theme/ayu-vim' | let ayucolor = "light"

    call plug#end()
endif

function! s:Plugin(plugname)
    return !empty(glob(g:plug_dir . '/' . a:plugname))
endfunction
" }}}

" Pretty things up! {{{
    let s:light_theme = 'ayu'
    let s:dark_theme = 'night-owl'

    function! DarkTheme()
        set bg=dark
        execute 'colorscheme ' . s:dark_theme
    endfunction
    function! LightTheme()
        set bg=light
        execute 'colorscheme ' . s:light_theme
    endfunction

    augroup color_utils
        autocmd!
        " highlight trailing whitespace
        autocmd ColorScheme * hi TrailingWhitespace ctermbg=DarkMagenta guibg=DarkMagenta
        autocmd ColorScheme * match TrailingWhitespace /\s\+\%#\@<!$/
        " never set guifg on cursorline, so that syntax highlighting can take
        " place on the current line
        autocmd ColorScheme * hi CursorLine guifg=NONE
        autocmd ColorScheme * hi MatchParen gui=bold,underline guifg=NONE guibg=NONE
        autocmd ColorScheme * hi Comment gui=italic
        autocmd ColorScheme * source ~/.vim/cherryline.vim
        " highlight more words just like TODO
        autocmd ColorScheme * syntax match Todo "\<\(BUG\|DEBUG\|NOTE\)\>"
        " specific modifications because forking would be more work
        autocmd ColorScheme night-owl hi CursorLine guibg=#111a33
        autocmd ColorScheme lucid     hi CursorLine guibg=#181320
                                  \ | hi Error gui=undercurl guifg=#cc6666 guibg=#333333
    augroup END

    call DarkTheme()
    " if !exists('g:colors_name')
    "     if strftime("%H") >= 7 && strftime("%H") < 17
    "         call LightTheme()  " daytime theme
    "     else
    "         call DarkTheme()  " nighttime theme
    "     endif
    " endif

    set guioptions=""
    set guifont=Iosevka\ 14

    " folding {{{
        function! s:TrimWhitespace(str)
            let l:start = 0
            let l:end = len(a:str) - 1
            while a:str[l:start] == ' '
                let l:start = l:start + 1
            endwhile
            while a:str[l:end] == ' '
                let l:end = l:end - 1
            endwhile
            return a:str[l:start : l:end]
        endfunction

        function! FoldText()
            let l:line = getline(v:foldstart)

            " the indendation will be added back on, but is temporarily removed for ease of parsing
            let l:line = s:TrimWhitespace(l:line)
            " hide the foldmarker, if present
            let l:mark = matchstr(&foldmarker, '[^,]*')
            if &foldmethod ==# 'marker'
                if l:line[-len(l:mark):] ==# l:mark
                    let l:line = l:line[:-len(l:mark) - 1]
                endif
            endif
            " hide &commentstring at the start of a comment, if present
            if match(&commentstring, '^.*%s$') != -1
                let l:comment_start = matchstr(&commentstring, '.*\ze%s')
                let l:comment_end = matchstr(&commentstring, '%s\zs.*')
                let l:line = matchstr(l:line, '\(' . l:comment_start . '\)\?\zs.*\ze\(' . l:comment_end . '\)\?')
            endif
            let l:line = s:TrimWhitespace(l:line)

            let l:lpadding = matchstr(getline(v:foldstart), '\s*')

            " minus 1 is to exclude the end marker as well
            let l:length = v:foldend - v:foldstart - 1
            let l:plural = l:length > 1 ? 's' : ''
            return l:lpadding .  '<*> ' . l:line . ' (' . l:length . ' line' . l:plural . ' hidden)'
        endfunction
        set foldtext=FoldText()
        set fillchars=fold:\ 
    " }}}

" }}}

" Integration with external programs {{{
    let g:netrw_browsex_viewer = 'surf'
" }}}

" General tweaks {{{
    " misc options {{{
        filetype plugin indent on
        set wildmenu
        set incsearch
        set nohlsearch
        set expandtab
        set shiftwidth=4
        set textwidth=100
        set splitright
        set splitbelow
        set cursorline
        set noswapfile
        set termguicolors
        set diffopt+=vertical
        set tags=./.tags,./tags,tags
        set hidden
        set noshowmode
        set showcmd
        set tildeop
        set conceallevel=2
        set notimeout
        set path+=,/usr/local/include,/usr/local/include/glib-2.0,

        if has('nvim')
            set inccommand=nosplit
        endif

        " persistent undo
        set undofile
        set undodir=~/.vim/undo

        let g:netrw_liststyle=3
        let g:netrw_winsize=25
        let g:netrw_browse_alternative=4
        let g:netrw_altv=1
        let g:netrw_localrmdir='rm -r'

        " fake mouse-scrolling, meant for touchpads
        noremap <ScrollWheelUp> 3<C-e>
        inoremap <ScrollWheelUp> 3<C-e>
        noremap <ScrollWheelDown> 3<C-y>
        inoremap <ScrollWheelDown> 3<C-y>
    " }}}

    " convenience keybindings {{{
        nnoremap s <Nop>
        nnoremap S <Nop>

        let mapleader="\<Space>"
        " don't want cursor to move if I try to use a nonexistent leader sequence
        nnoremap <Space> <Nop>
        nnoremap <Leader>w :w<CR>
        nnoremap <silent> <Leader>q :call Quitbuf()<CR>

        function! Quitbuf()
            if len(getbufinfo({'buflisted':1})) > 1
                execute "bd"
            else
                execute "q"
            endif
        endfunction

        nnoremap 9 ^
        vnoremap 9 ^

        " single-line scroll keys I can remember
        noremap ( <C-y>
        noremap ) <C-e>

        " window navigation
        noremap <C-h> <C-w>h
        noremap <C-j> <C-w>j
        noremap <C-k> <C-w>k
        noremap <C-l> <C-w>l

        " buffer navigation
        nnoremap <silent> [<Space> :bp<CR>
        nnoremap <silent> ]<Space> :bn<CR>

        " show as much of a definition as possible
        nnoremap gd gdzt
        nnoremap <C-]> <C-]>zt

        if has('nvim')
            tnoremap <Esc> <C-\><C-n>
        endif

        noremap ; :
        noremap : ;
        noremap q; q:

        noremap <silent> [l :lprev<CR>
        noremap <silent> ]l :lnext<CR>
        noremap <silent> [e :cprev<CR>
        noremap <silent> ]e :cnext<CR>

        nnoremap ]z zj
        nnoremap zj ]z
        nnoremap [z zk
        nnoremap zk [z
        nnoremap z<CR> za

        noremap ' `
        noremap ` '

        cnoremap <M-;> ~/

        onoremap ie :normal! VggoG<CR>
        onoremap ae :normal! VggoG<CR>

        " indentation jumping, sometimes useful
        noremap <silent> <M-k> :call search('^'. matchstr(getline('.'), '\(^\s*\)') .'\%<' . line('.') . 'l\S', 'be')<CR>
        noremap <silent> <M-j> :call search('^'. matchstr(getline('.'), '\(^\s*\)') .'\%>' . line('.') . 'l\S', 'e')<CR>
    " }}}

    " more complex stuff {{{
        " auto-update tags file {{{
            augroup AutoUpdateTags
                autocmd!
                autocmd BufWritePost * if filereadable('.tags') | silent execute '!ctags -Rf .tags'
                               \ | elseif filereadable('tags') | silent execute '!ctags -Rf tags' | endif
            augroup END
        " }}}

        " (quick|loc)list {{{
            function! ToggleQuickfix()
                if exists('t:qf_open') && t:qf_open == 1
                    execute 'cclose'
                    let t:qf_open = 0
                else
                    execute 'copen'
                    let t:qf_open = 1
                endif
            endfunction

            function! ToggleLoclist()
                if exists('t:loclist_open') && t:loclist_open == 1
                    execute 'lclose'
                    let t:loclist_open = 0
                else
                    execute 'lopen'
                    let t:loclist_open = 1
                endif
            endfunction

            nnoremap <silent> <Leader>kk :call ToggleQuickfix()<CR>
            nnoremap <silent> <Leader>ll :call ToggleLoclist()<CR>
        " }}}

        " save new files with a shebang with +x permission {{{
            augroup autochmod
                autocmd!
                autocmd BufWritePre * if (!filereadable(expand('%'))) && (getline(1) =~ '^#!')
                            \ | let b:do_autochmod = 1
                            \ | endif
                autocmd BufWritePost * if exists('b:do_autochmod')
                            \ | silent! execute '!chmod +x ' .. expand('%')
                            \ | unlet b:do_autochmod
                            \ | endif
            augroup END
        " }}}

    " }}}

    " convert between comma-separated RGB and hex {{{
        " TODO provide better support for non-charwise selections or spanning multiple lines, also visual mode
        function! <SID>OpFuncReplaceInRange(content, start=[], end=[])
            let l:start = a:start
            let l:end = a:end
            if empty(a:start)
                let l:start = getpos("'[")
            endif
            if empty(a:end)
                let l:end = getpos("']")
            endif

            if type(l:start) != v:t_list
                throw 'l:start is not a list'
            endif
            if type(l:end) != v:t_list
                throw 'l:end is not a list'
            endif
            let [_, start_line, start_col, _] = l:start
            let [_, end_line, end_col, _] = l:end
            let l:length = abs(end_col - start_col) + 1

            if type(a:content) == v:t_string
                let l:content = a:content
            elseif type(a:content) == v:t_func
                let old_content = getregion(l:start, l:end)[0]
                try
                    let l:content = a:content(old_content)
                catch
                    return
                endtry
            else
                call error('OpFuncReplaceInRange content must be string or function')
            endif

            silent execute start_line .. ',' .. end_line .. 's/\%' .. start_col .. 'c\zs.\{' .. l:length .. '\}/' .. l:content
            call remove(l:start, 0)
            call cursor(l:start)
        endfunction

        nnoremap g08 <Cmd>let &opfunc = {-> <SID>OpFuncReplaceInRange({x -> pyeval('(lambda x: hex(x)[2:])(' .. x .. ')')})}<CR>g@
        nnoremap g80 <Cmd>let &opfunc = {-> <SID>OpFuncReplaceInRange({x -> pyeval('(lambda x: int(x, 16))("' .. x .. '")')})}<CR>g@
    " }}}

" }}}

" Filetype-specific {{{
    " plain text {{{
        function! MaximumLineLength(start, end)
            let i=a:start
            let max=0
            while i < a:end
                if max < strlen(getline(i))
                    let max=strlen(getline(i))
                endif
                let i+=1
            endwhile
            return max
        endfunction

        function! WholeParagraphFormatExpr(start, end, width)
            " Collapse the range onto one line
            silent execute a:start . ',' . a:end . 's/\n/ /'
            silent execute a:start . 's/ $//'
            let lines_inserted=0
            " While there are too-long lines, add newlines as appropriate
            while search('\v.{' . a:width . '}', 'n', a:start + lines_inserted) != 0
                silent execute a:start + lines_inserted . 's/\v^.{1,' . a:width . '}\zs /\r/'
                let lines_inserted += 1
            endwhile
        endfunction

        augroup filetype_txt
            autocmd!
            autocmd FileType text setlocal conceallevel=2
            autocmd FileType text highlight Italics gui=italic
            autocmd FileType text syntax match Italics /\<_.*_\>/
            autocmd FileType text setlocal formatexpr=ParagraphFormatExpr(v:lnum,v:lnum+v:count-1,80)
            autocmd FileType text setlocal breakat=\ \	-
            autocmd FileType text setlocal linebreak
            autocmd FileType text setlocal formatoptions 
            autocmd FileType text setlocal wrap
            autocmd FileType text setlocal textwidth=0
        augroup END
    " }}}

    " c/cpp {{{
        augroup c_indentation_style
            autocmd!
            autocmd FileType c,cpp setlocal cindent
            autocmd FileType c,cpp setlocal cinoptions=g-1
        augroup END

        function! Util__FirstFileReadable(files)
            for file in a:files
                if filereadable(file)
                    return file
                endif
            endfor
            return ''
        endfunction

        let s:c_src_exts = ['c', 'cc', 'cpp', 'c++']
        let s:c_header_exts = ['h', 'hpp']

        function! CSwitchHeader()
            let base = expand('%:r')
            let ext = expand('%:e')

            let src_files = map(copy(s:c_src_exts), {_, ext -> base .. '.' .. ext})
            let header_files = map(copy(s:c_header_exts), {_, ext -> base .. '.' .. ext})

            let target = ''

            if index(s:c_src_exts, ext) >= 0
                let target = Util__FirstFileReadable(header_files)
            elseif index(s:c_header_exts, ext) >= 0
                let target = Util__FirstFileReadable(src_files)
            endif

            if !empty(target)
                execute ':e ' . target
            endif
        endfunction

        augroup header_files
            autocmd!
            autocmd FileType c,cpp nnoremap <buffer> <silent> <BS> :call CSwitchHeader()<CR>
            " attempt to detect whether this .h is C or C++
            autocmd BufEnter *.h
                        \   if filereadable(expand('%:r') . '.c')
                        \ |       setlocal ft=c
                        \ | elseif filereadable(expand('%:r') . '.cc')
                        \ |     || filereadable(expand('%:r') . 'cpp')
                        \ |       setlocal ft=cpp
                        \ | endif
        augroup END
    " }}}

    " clojure {{{
        function! CljLispWords()
            for word in g:clojure_special_indent_words
                set lispwords+=word
            endfor
        endfunction

        augroup clj_gvars
            autocmd!
            autocmd FileType clojure let g:clojure_align_subforms=1
            autocmd FileType clojure set lispwords+=defspec,deftest,deftype,defrecord,reify,proxy,extend-type,extend-protocol,letfn,let,for,doto,doseq,fdef,fspec,if-let,when-let,if-not,when-not,when,loop,fn,ns,for,while,condp,cond->,cond->>,some->,some->>,as->,match,>defn,for-all
        augroup END
    " }}}

    " xresources format {{{
        augroup xresources_files
            autocmd!
            autocmd FileType xdefaults set commentstring=!\ %s
        augroup END
    " }}}

    " tex {{{
        let g:tex_conceal = 'b'

        augroup tex_is_not_plaintex
            autocmd!
            autocmd BufEnter *.tex set ft=tex

        augroup english_motions
            autocmd!
            autocmd FileType markdown,tex,latex,text,plaintex noremap <buffer><silent> j gj
            autocmd FileType markdown,tex,latex,text,plaintex noremap <buffer><silent> k gk
            autocmd FileType markdown,tex,latex,text,plaintex noremap <buffer><silent> 0 g0
            autocmd FileType markdown,tex,latex,text,plaintex noremap <buffer><silent> 9 g^
            autocmd FileType markdown,tex,latex,text,plaintex noremap <buffer><silent> $ g$
            autocmd FileType markdown,tex,latex,text,plaintex set breakindent showbreak=⥃\  breakindentopt=sbr,shift:4 linebreak
        augroup END
    " }}}

    " (github) markdown {{{
        augroup md_headers
            autocmd!
            autocmd FileType markdown inoremap <buffer> =<CR> <C-r>=getline(line('.') - 1)<CR><C-o>:s/./=/g<CR><C-o>o
            autocmd FileType markdown inoremap <buffer> -<CR> <C-r>=getline(line('.') - 1)<CR><C-o>:s/./-/g<CR><C-o>o
        augroup END
    " }}}

    " mips assembly {{{
        " use MIPS comments
        augroup filetype_asm
            autocmd!
            autocmd BufEnter *.s setlocal commentstring=#\ %s
            autocmd BufEnter *.s noremap <silent> <buffer> ]t :call search('^\ze\s*\w\+:')<CR>
            autocmd BufEnter *.s noremap <silent> <buffer> [t :call search('^\ze\s*\w\+:', 'b')<CR>
        augroup END
    " }}}

    " haskell {{{
    augroup haskell_cmds
        autocmd!
        autocmd FileType haskell setlocal makeprg=stack\ build
    augroup END
    "}}}

    " cabal files {{{
        function! CabalIndent()
            let l:curr_indent = match(getline('.'), '\S') " first nonwhitespace
            let l:prev_line = line('.') - 1
            " decrement l:prev_line until it encounters non-whitespace
            while match(l:prev_line, '\S') == -1 && l:prev_line > 0
                let l:prev_line = l:prev_line - 1
            endwhile

            let l:prev_nonempty = getline(l:prev_line)
            let l:prev_indent = match(l:prev_nonempty, '\S')

            if l:prev_line > 0 && l:prev_indent == l:curr_indent
                        \ && match(l:prev_nonempty, '^\(library\|executable\|test-suite\)\>') == -1
                let l:next_col = match(l:prev_nonempty, ':\s*\zs\S')
                return repeat(' ', l:next_col - col('.') + 1)
            else
                return repeat(' ', &shiftwidth)
            endif
        endfunction

        augroup cabalfile_indent
            autocmd!
            autocmd FileType cabal inoremap <expr> <buffer> <Tab> CabalIndent()
        augroup END
    " }}}

" }}}

" Plugin configuration {{{

    if s:Plugin('vim-lsp') " {{{
        hi DiagnosticError gui=undercurl,italic guifg=#e6495b
        hi DiagnosticWarn  gui=undercurl,italic guifg=#cac641
        hi link LspErrorHighlight DiagnosticError
        hi link LspWarningHighlight DiagnosticWarn

        let g:lsp_diagnostics_echo_cursor = 1
        let g:lsp_diagnostics_echo_delay = 0
        let g:lsp_diagnostics_float_insert_mode_enabled = 0
        let g:lsp_diagnostics_highlights_delay = 20
        let g:lsp_diagnostics_signs_enabled = 0
        let g:lsp_diagnostics_virtual_text_enabled = 0
        let g:lsp_document_code_action_signs_enabled = 0
        let g:lsp_document_highlight_enabled = 0
        let g:lsp_diagnostics_highlights_insert_mode_enabled = 0

        function! LspBufferInit()
            nmap <buffer> <C-]> <Plug>(lsp-definition)zt
            nmap <buffer> gd <Plug>(lsp-definition)zt
            nmap <buffer> gD <Plug>(lsp-declaration)zt
            nmap <buffer> [k <Plug>(lsp-previous-diagnostic-nowrap)
            nmap <buffer> ]k <Plug>(lsp-next-diagnostic-nowrap)
            nmap <buffer> # <Plug>(lsp-previous-reference-nowrap)
            vmap <buffer> # <Plug>(lsp-previous-reference-nowrap)
            nmap <buffer> * <Plug>(lsp-next-reference-nowrap)
            vmap <buffer> * <Plug>(lsp-next-reference-nowrap)
        endfunction
    endif " }}}

    if s:Plugin('vimtex') " {{{
        let g:vimtex_compiler_method = 'tectonic'
        let g:vimtex_view_method = 'zathura'
    endif " }}}

    if s:Plugin('vim-arpeggio') " {{{
        call arpeggio#load()
        Arpeggio imap jk <Esc>
        Arpeggio vmap jk <Esc>
        Arpeggio cmap jk <C-c>
        " if has('nvim')
            " Arpeggio tnoremap jk <C-\><C-n>
        " endif
    endif " }}}

    if s:Plugin('ale') " {{{
        let g:ale_linters={
                    \ 'c':['clang', 'flawfinder', 'gcc'],
                    \ 'cpp':['clang', 'gcc', 'cppcheck', 'cpplint'],
                    \ 'haskell': ['hlint', 'hdevtools', 'hfmt'],
                    \ 'asm': [],
                    \ 'tex': ['lacheck']
                    \}
        let g:ale_set_quickfix = 1
        let g:ale_set_loclist = 0
        let g:ale_open_list = 0
        let g:ale_set_highlights = 0 " I will do this manually so I can highlight more precisely
        let g:ale_set_signs = 0
        let g:ale_echo_msg_format = '(%linter%) %code: %%s'
        map <silent> [e <Plug>(ale_previous)
        map <silent> ]e <Plug>(ale_next)
        nmap <silent> ga <Plug>(ale_detail)

        function! ALEUnderlineErrors()
            let errors = ''
            let warnings = ''
            for error in ale#engine#GetLoclist(bufnr('%'))
                let lnum = error['lnum']
                let cnum = error['col']
                let this_line = '\%' . lnum . 'l\%' . cnum . 'c\zs\w*'  " matches the line numbered lnum, from first non-whitespace character
                if error['type'] ==# 'E'
                    let errors = errors . '\|' . this_line
                elseif error['type'] ==# 'W'
                    let warnings = warnings . '\|' . this_line
                endif
            endfor
            let errors = errors[2:]  " remove initial \|
            let warnings = warnings[2:]  " remove initial \|
            execute 'match ALEError /' . errors . '/'
            execute 'match ALEWarning /' . warnings . '/'
        endfunction

        augroup ale_do_linting
            autocmd!
            autocmd InsertLeave * call ale#Queue(0, 'lint_file')
            autocmd BufEnter * call ale#Queue(0, 'lint_file')
        augroup END

        augroup autoclose_loclist_quickfix
            autocmd!
            autocmd FileType qf noremap <buffer> <silent> q :q<CR>
            autocmd FileType qf match Error /error\ze:/
        augroup END

        augroup ale_underlining
            autocmd!
            autocmd User ALELint call ALEUnderlineErrors()
        augroup END
    endif " }}}

    if s:Plugin('fzf.vim') " {{{
        nnoremap <Leader>e :GFiles<CR>
        nnoremap <Leader>f :Files?<CR>
        nnoremap <Leader>h :Helptags<CR>

        augroup fzf_escape
            autocmd!
            autocmd FileType fzf inoremap <buffer> <C-[> <C-c>
            autocmd FileType fzf nnoremap <buffer> <C-[> <C-c>
        augroup END
    endif " }}}

    if s:Plugin('vim-commentary') " {{{
        nmap g// gcc
        nmap g/ gc
        vmap g/ gc
    endif " }}}

    if s:Plugin('vim-fugitive') " {{{
        nnoremap <Leader>gs <Cmd>Git<CR>
        nnoremap <Leader>gw <Cmd>Gwrite<CR>
        nnoremap <Leader>gr <Cmd>Gread<CR>
        nnoremap <Leader>g<Return> <Cmd>Git commit<CR>
        nnoremap <silent> <Leader>gu <Cmd>Git push<CR>

        augroup fugitive_bindings
            autocmd!
            autocmd FileType gitcommit nmap <buffer> j <C-n>
            autocmd FileType gitcommit nmap <buffer> k <C-p>
        augroup END
    endif " }}}

    if s:Plugin('vim-gitgutter') " {{{
        let g:gitgutter_map_keys = 0
        nnoremap [g :GitGutterPrevHunk<CR>
        nnoremap ]g :GitGutterNextHunk<CR>
        nnoremap <Leader>gpw :GitGutterStageHunk<CR>
        nnoremap <Leader>gpr :GitGutterUndoHunk<CR>
    endif " }}}

    if s:Plugin('gitsigns.nvim') " {{{
        lua require('gitsigns').setup {
                    \ on_attach = function(bufnr)
                    \ local gitsigns = package.loaded.gitsigns
                    \ vim.keymap.set('n', ']g', function() gitsigns.next_hunk{wrap=false} end, {buffer=bufnr})
                    \ vim.keymap.set('n', '[g', function() gitsigns.prev_hunk{wrap=false} end, {buffer=bufnr})
                    \ vim.keymap.set('n', '<leader>gpw', gitsigns.stage_hunk, {buffer=bufnr})
                    \ vim.keymap.set('n', '<leader>gpr', gitsigns.reset_hunk, {buffer=bufnr})
                    \ end
                    \ }
    endif " }}}

    if s:Plugin('vim-easymotion') " {{{
        let g:EasyMotion_verbose = 0
        let g:EasyMotion_do_mapping = 0
        let g:EasyMotion_use_migemo = 1
        map , <Plug>(easymotion-bd-w)
    endif " }}}

    if s:Plugin('conjure') " {{{
        let g:conjure#highlight#enabled = 1
        let g:conjure#eval#result_register = '"'
        let g:conjure#filetypes = ["clojure", "fennel", "janet", "hy", "julia", "racket", "scheme", "lua", "lisp", "python", "sql"]
    endif " }}}

    if s:Plugin('hop.nvim') " {{{
        lua require'hop'.setup()
        map , <Cmd>HopWord<CR>
        highlight! link HopNextKey HopNextKey1
    endif " }}}

    if s:Plugin('vim-buftabline') " {{{
        let g:buftabline_numbers=1
    endif " }}}

    if s:Plugin('tagbar') " {{{
        let g:tagbar_compact = 1
        let g:tagbar_autofocus = 1
        let g:tagbar_vertical = 10
        let g:tagbar_position = 'botright vertical'
        " filetypes that gO works on by default
        let s:gO_types = ['help', 'Man']

        augroup TagbarMap
            autocmd!
            autocmd FileType * if index(s:gO_types, &filetype) == -1 | nnoremap <buffer> <silent> gO :Tagbar jfc<CR> | endif
        augroup END
    endif " }}}

    if s:Plugin('vim-sandwich') " {{{
        call operator#sandwich#set('all', 'all', 'highlight', 0)
        let g:sandwich_no_default_key_mappings = 1
        let g:textobj_sandwich_no_default_key_mappings = 1
        let g:operator_sandwich_no_default_key_mappings = 1
        map s <Plug>(operator-sandwich-add)
        map s <Plug>(operator-sandwich-add)
        map sc <Plug>(operator-sandwich-replace)<Plug>(textobj-sandwich-query-a)
        map sd <Plug>(operator-sandwich-delete)<Plug>(textobj-sandwich-query-a)

        omap is <Plug>(textobj-sandwich-auto-i)
        vmap is <Plug>(textobj-sandwich-auto-i)
        omap as <Plug>(textobj-sandwich-auto-a)
        vmap as <Plug>(textobj-sandwich-auto-a)

        nmap ss <Plug>(operator-sandwich-add)<Plug>(textobj-sandwich-auto-i)
        nmap scs <Plug>(operator-sandwich-replace)<Plug>(textobj-sandwich-auto-a)
        nmap sds <Plug>(operator-sandwich-delete)<Plug>(textobj-sandwich-auto-a)

        let g:sandwich#recipes = deepcopy(g:sandwich#default_recipes)
        let g:sandwich#recipes += [{'buns': ['“', '”'], 'input': ['q']}]
    endif " }}}

    if s:Plugin('vim-salve') " {{{
        let g:salve_auto_start_repl = 1
    endif " }}}

    if s:Plugin('pear-tree') " {{{
        " let g:pear_tree_smart_openers = 1
        " let g:pear_tree_smart_closers = 1
        let g:pear_tree_ft_disabled = ['clojure', 'lisp']
        let g:pear_tree_smart_backspace = 1
        let g:pear_tree_pairs = {
                    \ '"': {'closer': '"'},
                    \ "'": {'closer': "'"},
                    \ '(': {'closer': ')'},
                    \ '[': {'closer': ']'},
                    \ '{': {'closer': '}'},
                    \ '{-': {'closer': '-}'},
                    \ '{-#': {'closer': '#-}'},
                    \ }
    endif " }}}

    if s:Plugin('vim-textobj-quote') " {{{
        augroup textobj_quote
            autocmd!
            autocmd FileType markdown,text call textobj#quote#init()
        augroup END

        if s:Plugin('pear-tree') " autoclosing quotes interferes with smartquotes
            let g:pear_tree_ft_disabled += ['text', 'markdown']
        endif
    endif " }}}

    if s:Plugin('neoscroll.nvim') " {{{
        lua require('neoscroll').setup({
                    \ easing_function = 'cubic',
                    \ mappings = {'<C-u>', '<C-d>'},
                    \ cursor_scrolls_alone = false,
                    \ })
    endif " }}}

    if s:Plugin('nvim-treesitter-context') " {{{
        lua require('treesitter-context').setup {
                    \ mode = 'topline'
                    \ }
        augroup disable_tscontext
            autocmd!
            autocmd Filetype clojure TSContextDisable
        augroup END
    endif " }}}
" }}}

" vim: foldmethod=marker
