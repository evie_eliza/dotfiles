#!/usr/bin/zsh

fzf-files-widget() {
    fd --type f | fzf
}

fzf-dirs-widget() {
    fd --type d | fzf
}

zle -N fzf-files-widget
zle -N fzf-dirs-widget

zle '\e/' fzf-files-widget

zle '\e,' fzf-dirs-widget
