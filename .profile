#!/usr/bin/sh

export VISUAL=nvim
export EDITOR=$VISUAL
export PATH="$HOME/.local/bin:$HOME/.gem/ruby/2.5.0/bin:$PATH"

export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"

export SSH_AUTH_SOCK="$XDG_RUNTIME_DIR/ssh-agent.socket"

export LSCOLORS='ExfxcxdxDxeabcbdacad'
export LS_COLORS='di=1;34:ln=35:so=32:pi=33:ex=1;33:bd=34;40:cd=31;42:su=31;43:sg=30;42:tw=30;43'

export _JAVA_OPTIONS="-Ddk.gtk.version=3 -Dswing.defaultlaf=com.sun.java.swing.plaf.gtk.GTKLookAndFeel -Dawt.useSystemAAFontSettings=on"
